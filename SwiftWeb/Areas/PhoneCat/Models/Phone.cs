﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftWeb.Areas.PhoneCat.Models
{
    public class Phone
    {
        public int Age;
        public string Id;
        public string ImageUrl;
        public string Name;
        public string Carrier;
        public string Snippet;
    }
}