﻿using System.Web.Mvc;

namespace SwiftWeb.Areas.PhoneCat
{
    public class PhoneCatAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PhoneCat";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PhoneCat_default",
                "PhoneCat/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}