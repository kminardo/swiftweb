﻿angular.module('main')
    .controller('PhoneDetailController', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
            $http.get('/api/phonelist/' + $routeParams.phoneId).success(function(data) {
                $scope.phone = data;
            });
    }]);