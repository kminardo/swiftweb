﻿angular.module('main')
    .controller('PhoneListCtrl', ['$scope', '$http', function ($scope, $http) {
        $http.get('/api/PhoneList').success(function (data) {
            $scope.phones = data;
        });

        $scope.orderProp = 'age';
    }]);