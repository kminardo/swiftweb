﻿I have rebuilt Google's "PhoneCat" tutorial in a way that I think makes sense in the MVC Pattern. We are utilizing server side Models, Views and Controllers to handle data and flow.

I got lazy towards the end though and didn't bother to fully build out the Phone Details page. 
It's just an highly nested object and would be easy to recreate with a proper database backing the project. Feel free to build it out if you want.

I also skipped over the animations. Really not important to this prototype.

If you build either of these features out please give me a pull request!