﻿using System.Web.Mvc;

namespace SwiftWeb.Areas.NPRPlayer
{
    public class NPRPlayerAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "NPRPlayer";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "NPRPlayer_default",
                "NPRPlayer/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}