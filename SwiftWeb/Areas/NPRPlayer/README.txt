﻿This is a rebuild of NG-Newsletter's Begginer to Expert series in a way that fits with microsoft's MVC pattern. ( http://www.ng-newsletter.com/posts/beginner2expert-how_to_start.html )

In this tutorial we are building out an NPR player that links up to their API, pulls programs and plays them back.

It also features rudimentary Pause/Play support.

This is missing several features that I didn't feel were important to the MVC prototype. If you expand/build this out further please give me a pull request!