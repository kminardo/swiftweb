﻿//NPR Stuff
var apiKey = 'MDEyOTE3MTQxMDEzODg4NjgyODE4NTU4NQ001',
    nprUrl = 'http://api.npr.org/query?id=61&fields=relatedLink,title,byline,text,audio,image,pullQuote,all&output=JSON',
    mp4url = 'http://pd.npr.org/npr-mp4/npr/sf/2013/07/20130726_sf_05.mp4?orgId=1&topicId=1032&ft=3&f=61';

angular.module('main')
    .controller('MainController', ['$scope', '$http', function ($scope, $http) {
        $scope.playing = false;
        $scope.currentTrack = "Nothing";
        $scope.audio = document.createElement('audio');

        $scope.changeTrack = function (program) {
            if ($scope.playing) {
                $scope.audio.pause();
            }

            $scope.currentTrack = program.title.$text;
            var url = program.audio[0].format.mp4.$text;
            $scope.audio.src = url;

            $scope.play();
        };

        $scope.play = function () {
            if ($scope.audio.src == "")
            {
                alert("Please select a track");
                return false;
            }

            $scope.audio.play();
            $scope.playing = true;
        };

        $scope.stop = function () {
            $scope.audio.pause();
            $scope.playing = false;
        };

        $scope.audio.addEventListener('ended', function () {
            $scope.$apply(function () {
                $scope.stop()
            });
        });

        $http({
            method: 'JSONP',
            url: nprUrl + '&apiKey=' + apiKey + '&callback=JSON_CALLBACK'
        }).success(function (data, status) {
            $scope.programs = data.list.story;
        }).error(function (data, status) {
            // Some error occurred... who cares.
        });
    }]);