﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwiftWeb.Areas.UsersPage.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /UsersPage/User/
        public ActionResult Index()
        {
            ViewBag.Title = "User Testpage";
            return View();
        }

        public ActionResult Demo()
        {
            return View();
        }

        public ActionResult DemoAdd()
        {
            return View();
        }

        public ActionResult Main()
        {
            return View();
        }
	}
}