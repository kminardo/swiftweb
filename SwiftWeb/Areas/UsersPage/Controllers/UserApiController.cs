﻿using SwiftWeb.Areas.UsersPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwiftWeb.Areas.UsersPage.Controllers
{
    //TODO: Better name for this. api/userapi looks stupid.
    public class UserApiController : ApiController
    {
        static readonly UserData userData = new UserData();

        public List<User> Get()
        {
            // Return a static list of users
            return userData.GetAll();
        }

        public User Post(User user)
        {
            //Add the user
            userData.Add(user);
            return user;
        }

        public User Put(User user)
        {
            //Update the user
            userData.Update(user);
            return user;
        }

        public void Delete(int id)
        {
            //Remove the user
            userData.Delete(id);
        }
    }
}
