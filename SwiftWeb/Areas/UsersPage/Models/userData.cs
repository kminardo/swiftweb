﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftWeb.Areas.UsersPage.Models
{
    //To lazy for database...
    public class UserData
    {
        private List<User> users = new List<User>();
        private int nextId = 1;


        public UserData()
        {
            Add(new User{Id=1, FirstName="John", LastName="Smith", Gender=Gender.Male,Mobile="9999999991",Email="john@demo.com", City="kn", State="as", Country="usa",Zip="12401"});
            Add(new User{Id=2, FirstName="Adam", LastName="Gril",Gender=Gender.Female, Mobile="9999999992",Email="adam@demo.com", City="bk",State="al", Country="usa",Zip="99701"});
            Add(new User{Id=3, FirstName="James", LastName="Franklin",Gender=Gender.Male, Mobile="9999999993",Email="james@demo.com", City="js",State="nj", Country="usa",Zip="07097"});
            Add(new User {Id = 4, FirstName = "Vicky", LastName = "Merry", Gender = Gender.Female, Mobile = "9999999994", Email = "vicky@demo.com", City = "ol", State = "ny", Country = "usa", Zip = "14760" });
            Add(new User{Id=5, FirstName="Cena", LastName="Rego",Gender=Gender.Male, Mobile="9999999995",Email="cena@demo.com", City="as",State="tx", Country="usa",Zip="78610"});
        }

        public List<User> GetAll()
        {
            return users;
        }

        public User Add(User user)
        {
            user.Id = nextId++;
            users.Add(user);
            return user;
        }

        public User Update(User user)
        {
            int index = users.FindIndex(x=> x.Id == user.Id);
            users[index] = user;
            return user;
        }

        public void Delete(int userId)
        {
            if (userId != null)
            {
                int index = users.FindIndex(x => x.Id == userId);
                users.RemoveAt(index);
            }
        }
    }
}