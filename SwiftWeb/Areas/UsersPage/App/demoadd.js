﻿
appRoot.controller('DemoAddController', function ($scope, $location, $resource) {

    var userResource = $resource('/api/userapi', {}, { save: { method: 'POST' } });

    $scope.newUser = {
        gender: false
    };

    $scope.addUser = function (user) {
        userResource.save(user, function (updatedUser) {
            $scope.newUser = {};
        })
    };

    $scope.countryList = [
        {
            name: 'USA', id: 'usa', states: [
                { name: 'Alabama', id: 'al', cities: [{ name: 'Alabaster', id: 'al' }, { name: 'Arab', id: 'ar' }, { name: 'Banks', id: 'bk' }] },
                { name: 'Alaska', id: 'as', cities: [{ name: 'Lakes', id: 'lk' }, { name: 'Kenai', id: 'kn' }, { name: 'Gateway', id: 'gw' }] },
                { name: 'New Jersey', id: 'nj', cities: [{ name: 'Atlanta', id: 'at' }, { name: 'Jersey', id: 'js' }, { name: 'Newark', id: 'nw' }] },
                { name: 'New York', id: 'ny', cities: [{ name: 'Kingston', id: 'kg' }, { name: 'Lockport', id: 'lp' }, { name: 'Olean', id: 'ol' }] },
                { name: 'Texas', id: 'tx', cities: [{ name: 'Dallas', id: 'dl' }, { name: 'Austin', id: 'as' }, { name: 'Houston', id: 'hs' }] }]
        }
    ];

    $scope.clearCityAndZip = function () {
        $scope.newUser.city = null;
        $scope.newUser.zip = "";
    }

    $scope.$watch('newUser.state', function (selectedStateId) {
        if (selectedStateId) {
            angular.forEach($scope.countryList[0].states, function (state) {
                if (selectedStateId == state.id) {
                    $scope.selectedState = state;
                }
            })
        }
    });

    var init = function () {

    }

    init();
});