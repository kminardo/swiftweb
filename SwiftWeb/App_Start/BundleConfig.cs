﻿using System.Web;
using System.Web.Optimization;

namespace SwiftWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-ng-grid.js",
                        "~/Scripts/angular-resource.js",
                        "~/Scripts/angular-route.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/bootstrap-responsive.css"
                        ));

            #region UserBundles
            bundles.Add(new StyleBundle("~/User/css").Include(
                        "~/Areas/UsersPage/Content/ng-grid.css",
                        "~/Areas/UsersPage/Content/site.css"));

            bundles.Add(new ScriptBundle("~/User/app").Include(
                        "~/Areas/UsersPage/App/*.js"));
            #endregion

            #region NPRBundles
            bundles.Add(new StyleBundle("~/NPR/css").Include(
                        "~/Areas/NPRPlayer/Content/*.css"));

            bundles.Add(new ScriptBundle("~/NPR/app").Include(
                        "~/Areas/NPRPlayer/App/*.js"));
            #endregion

            #region PhoneCatBundles
            bundles.Add(new StyleBundle("~/PhoneCat/css").Include(
                        "~/Areas/PhoneCat/Content/*.css"));

            bundles.Add(new ScriptBundle("~/PhoneCat/app").Include(
                        "~/Areas/PhoneCat/App/*.js"));
            #endregion
        }
    }
}
