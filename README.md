﻿# About
This work is derivative of [Technovert's AngularJS Pattern for MVC4](http://visualstudiogallery.msdn.microsoft.com/cc6c9c5f-2846-4822-899f-a6c295cd4f2b). I have broken it out even further by separating Angular apps into their own areas allowing the creation of many separate, fully independent SPAs.

Part of my goal was to also have a single shared view that could serve as a header linking these areas together. The header could be taken advantage of to pass data between the SPAs using HTML5's LocalStorage, Cookies, or even just hiding elements inside it.

Each Area has can have it's own set of CSS, JS, and other content. It can also have it's own shared template ON TOP of the global shared template, although I don't have examples demonstrating that at this time.

Most of the work in this project is based on the work of others through tutorials and other learning sources, but I think I managed to use it in a way that really makes building a collection of SPAs super simple, and following patterns MVC developers will already understand. Please see the README in each Area for information regarding that application.

Hopefully someone will find it useful :)

If you have any suggestions or pull requests/contributions please do not hesitate to get in contact with me!

-- Ken Minardo